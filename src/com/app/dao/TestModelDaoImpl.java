package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.entity.TestModel;

@Repository("testModelDao")
@Transactional
public class TestModelDaoImpl implements TestModelDao{
	
	@PersistenceContext
	public EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public TestModel getRecord(String tcKn) {
		String hql = "from TestModel u where u.tcKn= :tcKn";
		try{
			return entityManager.createQuery(hql, TestModel.class).setParameter("tcKn", tcKn).getSingleResult();
		}catch(Exception e){
		}
		return null;
	}


}
