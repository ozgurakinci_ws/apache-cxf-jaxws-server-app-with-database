package com.app.dao;

import java.util.List;

import com.app.entity.TestModel;

public interface TestModelDao{
	public TestModel getRecord(String tcKn);
}
