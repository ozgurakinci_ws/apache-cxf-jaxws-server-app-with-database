package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.app.dao.TestModelDao;
import com.app.entity.TestModel;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("testModelService")
@Transactional
public class TestModelServiceImpl implements TestModelService {
	
	@Autowired
	@Qualifier("testModelDao")
	private TestModelDao testModelDao;
	
	@Override
	public TestModel getRecord(String tcKn) {
		return testModelDao.getRecord(tcKn);
	}

}
