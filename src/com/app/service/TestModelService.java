package com.app.service;

import java.util.List;

import com.app.entity.TestModel;

public interface TestModelService {
	public TestModel getRecord(String tcKn);
}
