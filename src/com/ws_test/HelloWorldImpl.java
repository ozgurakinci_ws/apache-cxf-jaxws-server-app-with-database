package com.ws_test;

import java.util.List;

import javax.annotation.PostConstruct;

/** 
* 	Author: ozgur.akinci
*	Created Date: 23.10.2019 14:15:00
*	Desc: 
*	Link: http://localhost:8080/ASALWebServices/services/HelloWorldService?wsdl
**/

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.app.entity.TestModel;
import com.app.service.TestModelService;


@WebService(endpointInterface = "com.ws_test.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
	
	@Resource
	WebServiceContext wsc; 
	
	@PostConstruct // for Autowired service injection 
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}
	
	@Autowired
	@Qualifier("testModelService")
	private TestModelService testModelService;
	
	

	@Override
	public TestModel sayHi(String tcKn) throws TestWSException{
		TestModel m = testModelService.getRecord(tcKn);
		try {
			if(HelloWorldAuthenticate.authenticate(wsc))
				return m;
			else
				throw new Exception("Invalid username or password!");
		} catch (Exception e) {
			throw new TestWSException("-1", e.getMessage());
		}
	}
}