package com.ws_test;

/** 
* 	Author: ozgur.akinci
*	Created Date: 23.10.2019 14:15:00
*	Desc: 
**/

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import com.app.entity.TestModel;

@WebService
public interface HelloWorld {

	TestModel sayHi(@XmlElement(required=true) @WebParam(name = "tcKn") String tcKn) throws TestWSException;

}