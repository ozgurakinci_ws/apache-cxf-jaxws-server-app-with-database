package com.ws_test;

/** 
* 	Author: ozgur.akinci
*	Created Date: 23.10.2019 14:15:00
*	Desc: 
**/

public class TestWSException extends Exception{
	private static final long serialVersionUID = 1L;
	private String errorCode;
	private String errorDesc;
	
	public TestWSException(String errorCode){
		super();
		this.errorCode = errorCode;
	}
	
	public TestWSException(String errorCode, String errorDesc){
		super();
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	
	
}
