package com.ws_test;

/** 
* 	Author: ozgur.akinci
*	Created Date: 23.10.2019 14:15:00
*	Desc: 
**/

import java.util.List;
import java.util.Map;

import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

public class HelloWorldAuthenticate {
	public static final String allowedUser = "admin";
	public static final String allowedPassword = "admin";
	
	@SuppressWarnings("unchecked")
	public static boolean authenticate(WebServiceContext wsContext) throws Exception{
		try{
			if(wsContext == null)
				throw new Exception("webservicecontext context is null.");
			MessageContext messageContext = wsContext.getMessageContext();
			if(messageContext == null)
				throw new Exception("message context is null.");
			Map<String, Object> httpHeaders = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
			List<String> userList = (List<String>) httpHeaders.get("username");
			List<String> passwordList = (List<String>) httpHeaders.get("password");
			
			String username = "";
			String password = "";
			
			if(userList != null && passwordList != null){
				username = userList.get(0);
				password = passwordList.get(0);
			}
			
			if(!allowedUser.equals(username) || !allowedPassword.equals(password)){
				return false;
			}
			return true;
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
